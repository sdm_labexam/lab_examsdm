const { request, response } = require('express')
const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

// get movie
router.get('/get/:movie_id', (request, response) => {

    const { movie_id } = request.params

    const query = `select * from movie`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))

    })

})

// post movie query

router.post('/post', (request, response) => {

    const { movie_title, movie_release_date, movie_time, director_name } = request.body

    const query = `
INSERT INTO movie(movie_title, movie_release_date, movie_time, director_name )values('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))

    })

})

//update movie

router.update('/update/:movie_id', (request, response) => {
    const { movie_id } = request.params
    const { movie_release_date, movie_time } = request.body

    const query = `update movie set movie_release_date ='${movie_release_date} ', movie_time = '${movie_time}' where movie_id=${movie_id}`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))

    })

})

//delete movie
router.delete('/delete/:movie_id', (request, response) => {
    const { movie_id } = request.params
    const query = `delete from movie where movie_id=${movie_id}`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))

    })

})

module.exports=router