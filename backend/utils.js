
function createResult(error, data) {
    const result = {}
    if (error) {
        result['status'] = 'error'
        result['result'] = error
    }


    else {
        result['status'] = 'success'
        result['result'] = data
    }
    return result
}

module.exports={
    createResult,
}