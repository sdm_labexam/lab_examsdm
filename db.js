const mysql = require('mysql2')

const pool =mysql.createPool({
user:'root',
host:'localhost',
password:'root',
database:'movielist',
port:3306,
waitForConnections:true,
connectionLimit:10,
queueLimit:0
})

module.exports=pool