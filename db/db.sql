-- movie_id, movie_title, movie_release_date,movie_time
create Table movie(movie_id INT PRIMARY KEY AUTO_INCREMENT, movie_title VARCHAR(100), movie_release_date DATE ,movie_time TIME,director_name VARCHAR(50));

insert INTO movie(movie_title, movie_release_date, movie_time, director_name )values( 'gulam', '1996-05-12' , '10:30' ,'ganesh ' );
insert INTO movie(movie_title, movie_release_date, movie_time, director_name )values( 'happy moment', '2004-05-12' , '12:30' ,'happy bhai ' );
insert INTO movie(movie_title, movie_release_date, movie_time, director_name )values( 'super30', '2012-05-12' , '1:30' ,' subhash ' );
